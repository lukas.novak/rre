#Write-Host "Starding PowerShell with elevated priviliges"
#
#if (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
#{
#    $arguments = "& '" +$myinvocation.mycommand.definition + "'"
#    Start-Process powershell -Verb runAs -ArgumentList $arguments
#    Break
#}


Write-Host "Checking whether Docker is installed"

# the following command goes from here: https://www.codegrepper.com/code-examples/shell/powershell+check+if+software+is+installed

$software = "Docker Desktop";
$installed = (Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Where { $_.DisplayName -eq $software }) -ne $null
If(-Not $installed) {
    # the following commands goes from here: https://www.powershellgallery.com/packages/AppVeyorBYOC/1.0.154-beta/Content/scripts%5CWindows%5Cinstall_docker_desktop.ps1
    Write-Host "'$software' is NOT installed.";
    Write-Host "Installing Docker Desktop 2.1.0.5"

    #choco install docker-desktop

    Write-Host "Downloading..."
    $exePath = "$env:TEMP\Docker-Desktop-Installer.exe"
    (New-Object Net.WebClient).DownloadFile('https://download.docker.com/win/stable/40693/Docker%20Desktop%20Installer.exe', $exePath)

    Write-Host "Installing..."
    cmd /c start /wait $exePath install --quiet
    del $exePath

    Write-Host "Docker Desktop installed" -ForegroundColor Green

    Write-Host "Creating DockerExchange user..."
    net user DockerExchange /add

    Write-Host "Installing docker-appveyor PowerShell module..."

    $dockerAppVeyorPath = "$env:USERPROFILE\Documents\WindowsPowerShell\Modules\docker-appveyor"
    New-Item $dockerAppVeyorPath -ItemType Directory -Force

    Copy-Item "$env:TEMP\docker-appveyor.psm1" -Destination $dockerAppVeyorPath

    Remove-Module docker-appveyor -ErrorAction SilentlyContinue
    Import-Module docker-appveyor

    $UserModulesPath = "$($env:USERPROFILE)\Documents\WindowsPowerShell\Modules"
    $PSModulePath = [Environment]::GetEnvironmentVariable('PSModulePath', 'Machine')
    if(-not $PSModulePath.contains($UserModulesPath)) {
        [Environment]::SetEnvironmentVariable('PSModulePath', "$PSModulePath;$UserModulesPath", 'Machine')
    }

    Write-Host "Mapping docker-switch-windows.cmd to Switch-DockerWindows..."

    @"
@echo off
powershell -command "Switch-DockerWindows"
"@ | Set-Content -Path "$env:ProgramFiles\Docker\Docker\resources\bin\docker-switch-windows.cmd"

    Write-Host "Mapping docker-switch-linux.cmd to Switch-DockerLinux..."

    @"
@echo off
powershell -command "Switch-DockerLinux"
"@ | Set-Content -Path "$env:ProgramFiles\Docker\Docker\resources\bin\docker-switch-linux.cmd"

    Write-Host "Finished the installation of Docker for Desktop";
} else {
    Write-Host "'$software' is installed."
}

# evabeling Microsoft-Windows features
function enabling_Windows_features {
    function Check_WindowsFeature
    {
        [CmdletBinding()]
        param(
            [Parameter(Position = 0, Mandatory = $true)] [string]$FeatureName
        )
        if ((Get-WindowsOptionalFeature -FeatureName $FeatureName -Online).State -eq "Enabled")
        {
            Write-Host "installed"
            # $FeatureName is Installed
            # (simplified function to paste here)
        }
        else
        {
            Write-Host "not installed"
            # Install $FeatureName
        }
    }

    # Enabling WSL2
    $SubsystemLinux_1_status = Check_WindowsFeature($FeatureName = "Microsoft-Windows-Subsystem-Linux")

    if ($SubsystemLinux_1_status -eq $installed) {
        Write-Host "Microsoft-Windows-Subsystem-Linux is installed."
    }
    else {
        dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
        Write-Host "Instalation of the Microsoft-Windows-Subsystem-Linux is finished"
    }

    #  turn on Virtual Machine Platform
    $VirtualMachinePlatform_1 = Check_WindowsFeature($FeatureName = "VirtualMachinePlatform")

    if ($VirtualMachinePlatform_1 -eq $installed) {
        Write-Host "Virtual Machine Platform is enabled."
    }
    else {
        dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
        Write-Host "Virtual Machine Platform is ebabled - restarting computer"
        Restart-Computer
    }
}

Write-Host "Checking whether Ubuntu is installed"

# obtain the value of the ID of the default Linux distribution (and store it in a variable to avoid escaping characters issues):
$DEFAULT_LXSS_ID = (Get-ItemPropertyValue -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\ -name DefaultDistribution)

# which will have a value like:
#echo  $DEFAULT_LXSS_ID

# display the directory containing the rootfs Windows directory (mapped to the / Linux directory)
# Get-ItemPropertyValue -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\$DEFAULT_LXSS_ID -name BasePath | Format-List -property "BasePath"


$installed_ubuntu = (Get-ItemPropertyValue -Path REGISTRY::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\ -name DefaultDistribution)
If($installed_ubuntu -eq $null) {
    Write-Host "Ubuntu seems not to be installed.";

    # install ubutu
    wsl --install -d Ubuntu

    Write-Host "Downloading..."
    $exePath_WSL = "$env:TEMP\wsl_update_x64.msi"
    (New-Object Net.WebClient).DownloadFile('https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi', $exePath_WSL)
    Write-Host "Installing WSL.."
    Start-Process $exePath_WSL -wait
    # cmd /c start /wait $exePath_WSL install --quiet
    del $exePath_WSL
    Write-Host "WSL package was installed" -ForegroundColor Green
} else {
    Write-Host "Ubuntu seems to be is installed."
}

#choco install docker-desktop

#Write-Host "Starting PowerShell"
#Start-Process powershell -Verb runAs -NoNewWindow

#Start-Process 'C:\Program Files\Docker\Docker\DockerCli.exe' -Verb runAs
#& 'C:\Program Files\Docker\Docker\DockerCli.exe' -SwitchDaemon

Write-Host "Starting Docker, please wait untill Docker fully starts...it can take a bit"
Start-Process "C:\Program Files\Docker\Docker\Docker Desktop.exe" -WindowStyle Minimized
# there is need some time before docker fully starts, on faster computers such amout of time is not necessary but in slower computers....
Start-Sleep -Seconds 20

# -WindowStyle Minimized

Write-Host "Short system sleeep to download image properly"

Start-Sleep -Seconds 2

Write-Host "Pulling the last published image ...it can take a bit"

docker pull user/project:latest 

Write-Host "Local server"
Start-Process http://localhost:8787


#Register-EngineEvent PowerShell.Exiting -Action {
#    $processes = Get-Process "*docker desktop*"
#    if ($processes.Count -gt 0)
#    {
#        $processes[0].Kill()
#        $processes[0].WaitForExit()
#    }
#}

Write-Host "Starting local server"
docker run --rm -it -e DISABLE_AUTH=true -p 8787:8787 user/project:latest



#PowerShell -NoExit



